module.exports = class extends think.Controller {
  indexAction(){
    this.body = 'hello word!';
  }
  async getlistAction() {
    const http_ = this.config('http_') == 1 ? 'http' : 'https';
    let http__;
    let classlist = [];
      
    //let cate_id = this.get('pid');
    let cate_id = 129;
    let data = await this.model("category").where({ pid: cate_id }).select();
    for(let v of data){
      let data = await this.model("document").field('id,title,uid,category_id,description,model_id,type,pics,price,cover_id').where({ category_id: v.id }).find();
      const imgarr = [];
      if (!think.isEmpty(data.pics)) {
        const pics = data.pics.split(',');
        for (const i of pics) {
          const pic = await get_pic(i, 1, 360, 240);
          if (pic.indexOf('//') == 0) {
            http__ = `${http_}:`;
          } else {
            http__ = `${http_}://${this.ctx.host}`;
          }
          imgarr.push(http__ + pic);
        }
      }
      data.imgurl = imgarr;
      classlist.push(data);
    }

    return this.success(classlist);
    
  }
  async getdetailAction() {
    const http_ = this.config('http_') == 1 ? 'http' : 'https';
    let http__;
    let cate_id = this.get('id');
    let data = await this.model("document").field('id,uid,title,category_id,description,model_id,type,pics,price,cover_id').where({ id: cate_id }).find();
    const imgarr = [];
    if (!think.isEmpty(data.pics)) {
      const pics = data.pics.split(',');
      for (const i of pics) {
        const pic = await get_pic(i, 1, 360, 240);
        if (pic.indexOf('//') == 0) {
          http__ = `${http_}:`;
        } else {
          http__ = `${http_}://${this.ctx.host}`;
        }
        imgarr.push(http__ + pic);
      }
    }
    data.imgurl = imgarr;
    let document_shop = await this.model('document_shop').where({ id: data.id }).find();
    data.content = document_shop.content;
    return this.success(data);
  }
}
  
  