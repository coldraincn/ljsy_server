module.exports = class extends think.Controller {
    async indexAction() {
        this.body = 'hello word!';
    }
    // 创建订单
    async createorderAction() {
        // let data = this.post();
        let order_no1 = await this.model('cmswing/order').orderid(this.post("user_id"));
        let map = {
            order_no: order_no1,
            user_id: this.post("user_id"),
            payment: 1,
            good_id: this.post("good_id"),
            order_amount: this.post("order_amount"),
            type: this.post("type"),

            //hroomorderIntime: this.post("hroomorderIntime"),
            //hroomorderOuttime: this.post("hroomorderOuttime")

        };
        let insertId = await this.model("order").add(map);
        return this.success(insertId);
    }
    // 获取用户订单列表
    async getlistbyuserAction() {
        // let data = this.post();
        let userid = this.get('userid');
        let orderdata;
        if (think.isEmpty(this.get('status'))) {
            orderdata = await this.model("order").field('order_no,user_id,payment,pay_status,good_id,order_amount').where({ user_id: userid }).select();
            for (let order of orderdata) {
                let myclass = await this.model('document').where({ id: order.good_id }).find();
                order.title = myclass.title;
            }
        } else {

            orderdata = await this.model("order").field('order_no,user_id,payment,pay_status,good_id,order_amount').where({ user_id: userid, pay_status: this.get('status') }).select();
            for (let order of orderdata) {
                let myclass = await this.model('document').where({ id: order.good_id }).find();
                order.title = myclass.title;
            }
        }

        return this.success(orderdata);
    }
    // 获取用户订单详情
    async getdetailAction() {
        // let data = this.post();
        let id = this.get('id');
        let orderdata;

        orderdata = await this.model("order").field('order_no,user_id,payment,pay_status,good_id,order_amount').where({ id: id }).find();

        let myclass = await this.model('document').where({ id: orderdata.good_id }).find();
        orderdata.title = myclass.title;



        return this.success(orderdata);
    }
        // 支付成功更新订单
        async updateorderAction() {
            let id = this.get('id');
            let map = {
                pay_status: "1"
            };
            let rows = await this.model('order').where({ id: id }).update(map);
            return this.success(rows);
        }
          //更新订单


}