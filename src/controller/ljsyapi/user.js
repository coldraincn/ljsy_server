var rongcloudSDK = require( 'rongcloud-sdk' );
module.exports = class extends think.Controller {

    /**
       * 注册页面
       */
    async registerAction() {
        if (this.isPost) {
            const data = this.post();
            // console.log(data);
            // 验证
            let res;
            if (think.isEmpty(data.username)) {
                return this.fail('用户昵称不能为空！');
            } else {
                res = await this.model('member').where({ username: ltrim(data.username) }).find();
                if (!think.isEmpty(res)) {
                    return this.fail('用户昵称已存在，请重新填写！');
                }
            }
            //   if (think.isEmpty(data.mobile)) {
            //     return this.fail('手机号码不能为空！');
            //   } else {
            //     res = await this.model('member').where({mobile: data.mobile}).find();
            //     if (!think.isEmpty(res)) {
            //       return this.fail('手机号码已存在，请重新填写！');
            //     }
            //   }
            //   if (think.isEmpty(data.email)) {
            //     return this.fail('电子邮箱不能为空！');
            //   } else {
            //     res = await this.model('member').where({email: data.email}).find();
            //     if (!think.isEmpty(res)) {
            //       return this.fail('电子邮箱已存在，请重新填写！');
            //     }
            //   }
            if (think.isEmpty(data.password)) {
                return this.fail('密码不能为空！');
            }
            //   else {
            //     if (data.password != data.password2) {
            //       return this.fail('两次输入的密码不一致，请重新填写！');
            //     }
            //   }
            //   if (data.clause != 'on') {
            //     return this.fail('必须要同意,网站服务条款');
            //   }

            data.status = 1;
            data.reg_time = new Date().valueOf();
            data.reg_ip = _ip2int(this.ip);
            data.password = encryptPassword(data.password);
            data.email = "111@111.com";
            const reg = await this.model('member').add(data);

            return this.success(reg);
        } else {


        }
    }
    //   登陆页面
    async login2Action() {

        // 用户账号密码验证
        const username = this.get('username');
        let password = this.get('password');
        password = encryptPassword(password);
        const res = await this.model('cmswing/member').signin2(username, password, this.ip, 1, 0);
        if (res.uid > 0) {
            // 记录用户登录行为
            // await this.model("action").log("user_login", "member", res.uid, res.uid, this.ip(), this.http.url);
            // console.log(111111111111121);
            // console.log(res);

            // TODO 用户密钥
            return this.success(res);
        } else { // 登录失败
            let fail;
            switch (res) {
                case -1:
                    fail = '用户不存在或被禁用';
                    break; // 系统级别禁用
                case -2:
                    fail = '密码错误';
                    break;
                default:
                    fail = '未知错误';
                    break; // 0-接口参数错误（调试阶段使用）
            }
            return this.fail(res, fail);
        }

    }
    //   登陆页面
    async loginAction() {
        rongcloudSDK.init( 'k51hidwqknscb', '8UqSR5GJGD' );

        if (this.isPost) {

            // 用户账号密码验证
            const username = this.post('username');
            let password = this.post('password');
            password = encryptPassword(password);
            const res = await this.model('cmswing/member').signin2(username, password, this.ip, 1, 0);
            if (res.uid > 0) {
                let gettoken = (id,name) => {
                    let deferred = think.defer();
                    rongcloudSDK.user.getToken( id, name, 'http://files.domain.com/avatar.jpg', function( err, resultText ) {
                        if( err ) {
                          // Handle the error
                        }
                        else {
                          var result = JSON.parse( resultText );
                          if( result.code === 200 ) {
                            //Handle the result.token
                            deferred.resolve(result.token);
                        
                          }
                        }
                      } );
                    return deferred.promise;
                  };
                  let re=await gettoken(res.uid,res.username);
                  res.token=re;
                return this.success(res);
            } else { // 登录失败
                let fail;
                switch (res) {
                    case -1:
                        fail = '用户不存在或被禁用';
                        break; // 系统级别禁用
                    case -2:
                        fail = '密码错误';
                        break;
                    default:
                        fail = '未知错误';
                        break; // 0-接口参数错误（调试阶段使用）
                }
                return this.fail(res, fail);
            }
        } else {


        }
    }
}