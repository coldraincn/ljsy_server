module.exports = class extends think.Controller {
    indexAction() {
        this.body = 'hello word!';
    }
    async getlistAction() {
        let hoteldata = [];

        let cate_id = this.get('pid');
        // if (!think.isEmpty(cate_id)) {
        let data = await this.model("category").where({ pid: cate_id }).select();
        for (let v of data) {
            let sort = await this.model("document").where({ category_id: v.id }).find();
            hoteldata.push(sort);
        }
        //   return this.success(sort)
        // }
        return this.success(hoteldata);
        //return this.body=hoteldata;
    }
    async getdetailAction() {

        let cate_id = this.get('id');
        // if (!think.isEmpty(cate_id)) {
        let data = await this.model("document").where({ id: cate_id }).find();

        //   return this.success(sort)
        // }
        return this.success(data);
    }
}